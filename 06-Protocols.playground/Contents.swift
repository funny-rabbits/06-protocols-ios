// MARK: Задание 1:
// Реализовать структуру IOSCollection и создать в ней copy on write по типу указанном в видео-уроке.
struct IOSCollection {
    var number = 2023
}

class Ref<T> {
    var value: T
    init(value: T) {
        self.value = value
    }
}

class Container<T> {
    var ref: Ref<T>
    init(value: T) {
        self.ref = Ref(value: value)
    }
    
    var value: T {
        get {
            ref.value
        }
        set {
            guard (isKnownUniquelyReferenced(&ref)) else {
                ref = Ref(value: newValue)
                return
            }
            
            ref.value = newValue
            
        }
    }
}

let id = IOSCollection()
let firstContainer = Container(value: id)
let secondContainer = firstContainer

secondContainer.value.number = 2024



// MARK: Задание 2:
// Создать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол.
protocol Hotel {
    var roomCount: Int { get }
    init(roomCount: Int)
}


class HotelAlfa: Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}

let hotel = HotelAlfa(roomCount: 100)
print(hotel.roomCount)



// MARK: Задание 3:
// Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, чтобы когда мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы увидели такую строку - 'Выпало 4 на кубике'.
protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        return self
    }
}

extension GameDice {
    var diceDescription: String {
        return "Выпало \(self.numberDice) на кубике"
    }
}

let diceCoub = 4
print(diceCoub.diceDescription)



// MARK: Задание 4:
// Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство
protocol MyProtocol {
    var requiredProperty: Int { get }
    var optionalProperty: String? { get set }
    
    func requiredMethod()
}

class MyClass: MyProtocol {
    let requiredProperty: Int
    
    var optionalProperty: String?
    
    init(requiredProperty: Int) {
        self.requiredProperty = requiredProperty
    }
    
    func requiredMethod() {
        print("функция работает")
    }
}



/*
    MARK: Задание 4:
    Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding(). Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web). Компании подключаем два этих протокола.

    Задача: вывести в консоль сообщения - 'разработка началась. пишем код <такой-то>' и 'работа закончена. Сдаю в тестирование', попробуйте обработать крайние случаи.
*/


// Протокол для работы с кодом
protocol CodingProtocol {
    var time: Int { get set }
    var codeCount: Int { get set }
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

// Протокол для остановки работы над кодом
protocol StopCodingProtocol {
    func stopCoding()
}

// Перечисление платформ
enum Platform {
    case ios
    case android
    case web
}

// Класс компании
class Company: CodingProtocol, StopCodingProtocol {
    var time: Int = 0
    var codeCount: Int = 0
    var numberOfProgrammers: Int = 0
    var specializations: [Platform: Int] = [:]
    
    // Функция начала работы над кодом
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        // Проверяем, есть ли достаточно специалистов для работы
        guard let numberOfSpecialists = specializations[platform], numberOfSpecialists >= numberOfSpecialist else {
            print("Не хватает специалистов для работы над \(platform)")
            return
        }
        // Начинаем работу над кодом
        time = 0
        codeCount = 0
        print("Разработка началась. Пишем код для \(platform)")
        // Имитируем работу над кодом
        while time < 8 {
            time += 1
            codeCount += numberOfSpecialist * 100
        }
        // Заканчиваем работу над кодом
        print("Работа закончена. Сдаю в тестирование")
    }
    
    // Функция остановки работы над кодом
    func stopCoding() {
        time = 0
        codeCount = 0
        print("Работа над кодом остановлена")
    }
}

// Пример использования
let company = Company()
company.numberOfProgrammers = 10
company.specializations = [.ios: 3, .android: 4, .web: 3]

company.writeCode(platform: .ios, numberOfSpecialist: 2)     // Разработка началась. Пишем код для ios
company.writeCode(platform: .android, numberOfSpecialist: 5) // Разработка началась. Пишем код для android
company.writeCode(platform: .web, numberOfSpecialist: 4)     // Не хватает специалистов для работы над web

company.stopCoding()                                         // Работа над кодом остановлена
